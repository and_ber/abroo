require 'net/http'
require 'uri'
require 'json'
require_relative '../utils/environment'

class GuardAPI

  @@http = Net::HTTP.new(URI.parse(API_HOST).host)

  def get_test_request_response_code
    puts 'get_test_request_response invoked, url=' + TEST_REQUEST_URL
    uri = URI.parse(TEST_REQUEST_URL)
    response = Net::HTTP.get_response(uri)
    response.code
  end

  def search_content(query)
    puts 'search_content invoked, query=%s' % query
    url = CONTENT_REQUEST_URL % query
    puts 'will make the following request=%s' % url

    response = @@http.request_get(url)

    # guardian returns data in non-utf encoding, converting
    response.body.force_encoding('UTF-8')
  end

end