require 'test/unit/testsuite'

require_relative '../tests/filtering/tc_content'

class TS_RegressionTestSuite < Test::Unit::TestSuite
  def self.suite
    result = self.new(self.class.name)
    result << TC_ContentFiltering.suite
    return result
  end

end