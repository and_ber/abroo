require 'test/unit/testsuite'

require_relative '../tests/healthcheck/tc_api'

class TS_SmokeTestSuite < Test::Unit::TestSuite
  def self.suite
    result = self.new(self.class.name)
    result << TC_API_HealthCheck.suite
    return result
  end

end