
DEFAULT_ENV = 'test'

env = ''
if ARGV[0].nil?
  env = DEFAULT_ENV
else
  env = ARGV[0]
end

puts 'env=' + env

require_relative "../conf/#{env}.rb"

