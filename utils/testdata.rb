

class TestData

  @@testdata_dir = '../resources/testdata/'

  def self.get_content(filename)
    puts 'get_content invoked, filename=%s' % filename
    fullname = File.join(__dir__, @@testdata_dir, filename)
    puts 'fullname=%s' % fullname

    if not File.exists?(fullname)
      raise 'TestData file does not exist'
    end
    IO.read(fullname)
  end

end