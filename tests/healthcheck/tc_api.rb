require 'test/unit'
require_relative '../../lib/api'

class TC_API_HealthCheck < Test::Unit::TestCase

  @@api = GuardAPI.new

  def test_api_availability
    response_code = @@api.get_test_request_response_code
    expected_rc = '200'
    assert_equal(expected_rc, response_code, 'Something went terribly wrong with the guardian!')
  end

end