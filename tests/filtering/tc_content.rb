require 'test/unit'
require_relative '../../lib/api'
require_relative '../../utils/testdata'

class TC_ContentFiltering < Test::Unit::TestCase

  @@api = GuardAPI.new


  def test_search_unique_result
    query_string = 'machine'
    response = @@api.search_content(query_string)

    expected_content = TestData.get_content('expected_content_search_unique_result.json')

    assert_equal(expected_content, response)

  end

  # TODO: not implemented yet
  # def test_search_multiple_result
  #   query_string = 'some multiple result string'
  #   response = @@api.search_content(query_string)
  #
  #   expected_content = TestData.get_content('expected_content_search_multiple_result.json')
  #
  #   assert_equal(expected_content, response)
  #
  # end
  #
  # def test_search_no_result
  #   query_string = 'some query string that gives no result'
  #   response = @@api.search_content(query_string)
  #
  #   expected_content = ''
  #
  #   assert_equal(expected_content, response)
  #
  # end

end